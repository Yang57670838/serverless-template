# Purpose

lambda read temporary JSON form cache in S3, access by API gateway <br/>
use python to read now, later switch to nodejs

## CDK contains

API gateway, Lambda, S3, IAM


## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
