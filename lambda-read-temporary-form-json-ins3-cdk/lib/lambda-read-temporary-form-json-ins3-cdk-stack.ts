import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as iam from 'aws-cdk-lib/aws-iam';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from "aws-cdk-lib/aws-apigateway";

export class LambdaReadTemporaryFormJsonIns3CdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const formCacheS3 = new s3.Bucket(this, 'formcache', {
      bucketName: "formcache",
    })

    // IAM service role
    const iamRoleForLambda = new iam.Role(this, "iamLambdaRole", {
      roleName: "lambdaReadS3FormCache",
      description: "role for lambda to fully access s3 bucket to read stored form cache json",
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com')
    })
    iamRoleForLambda.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonS3FullAccess'));

    // lambda
    const readFormCacheFunc = new lambda.Function(this, 'readFormCacheFunc', {
      handler: 'lambda_analyse_func.lambda_handler',
      runtime: lambda.Runtime.PYTHON_3_10,
      code: lambda.Code.fromAsset('../services/'),
      functionName: 'readFormCacheFunc',
      role: iamRoleForLambda
    })

    // api gateway
    const formCacheAPI = new apigateway.LambdaRestApi(this, 'formCacheAPI', {
      handler: readFormCacheFunc,
      restApiName: 'formCacheAPI',
      deploy: true,
      proxy: false,
    })
    const getCache = formCacheAPI.root.addResource('cache');
    getCache.addMethod('GET');  // GET /cache
  }
}
