import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class CdkTemplateStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'CdkTemplateQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
    const s3AccountingFiles = new s3.Bucket(this, 'accountingbucket', {
      bucketName: "company-a",
      versioned: false,
      publicReadAccess: false
    })
    const dynamoDBForAccounting = new dynamodb.Table(this, 'companySpendTable', {
      tableName: "companySpend",
      partitionKey: { name: 'id', type: dynamodb.AttributeType.STRING },
    });
    const pythonAnalyseFunc = new lambda.Function(this, 'pythonAnalyseFunc', {
      handler: 'lambda_analyse_func.pythonAnalyseHandler',
      runtime: lambda.Runtime.PYTHON_3_10,
      code: lambda.Code.fromAsset('../services/'),
      functionName: 'pythonAnalyseFunc'
    })
    // cloudwatch alarm for lambda Error
    const errorCloudwatchForLambda = new cloudwatch.Alarm(this, 'cloudwatchLambdAlarm', {
      evaluationPeriods: 1,
      threshold: 1,
      metric: pythonAnalyseFunc.metricErrors(),
      alarmDescription: 'Alarm if the SUM of Errors is greater than or equal to the threshold (1) for 1 evaluation period'
    })
  }
}
