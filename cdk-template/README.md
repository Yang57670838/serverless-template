

## usage
* `npm install -g aws-cdk`
* `npm run build`   compile typescript to js (ts check only)
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests

* `cdk bootstrap`   one time to bootstrap CDK for one account in one region..
* `cdk synth`       convert CloudFormation template files
* `cdk deploy`      deploy this stack to your default AWS account/region

* `cdk diff`        compare deployed stack with current state

* `cdk destroy`     destroy stacks!!



## 


## usage
auto0app stack, cognito auth app, S3 web app, ereadtc..

## TODO:
S3 add lifecycle rules or logics: <br/>
permanent delete form cache JSON after 1 week <br/>
permanent delete submitted account files from standard after zip <br/>
move zip from standard to S3 Glacier Flexible Retrieval class after finish work on it <br />
move zip from S3 Glacier Flexible Retrieval class to Deep Glacier after 1 year <br />
