## deploy
sls deploy --verbose <br />
sls deploy --stage production --verbose <br />

## remove
sls remove --verbose <br />
sls remove --stage production --verbose <br />

## TODO:
move all logic to AWS CDK