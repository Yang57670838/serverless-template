import AWS from "aws-sdk";

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function getCompanyRecordsFromDaytime(companyName, fromDatetime) {
  const params = {
      TableName: process.env.ANALYSE_TABLE_NAME,
      IndexName: "companyAndCreatedAt",
      KeyConditionExpression: "companyID = :id AND createdAt >= :fromWhichDatetime",
      ExpressionAttributeValues: {
          ":id": companyName,
          ":fromWhichDatetime": fromDatetime,
      },
  };
  const result = await dynamodb.query(params).promise();
  return result.Items;
}