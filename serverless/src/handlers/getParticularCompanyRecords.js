import AWS from "aws-sdk";
import commonMiddleware from "../lib/commonMiddleware";
import createError from "http-errors";

const dynamodb = new AWS.DynamoDB.DocumentClient();

const getParticularCompanyRecords = async (event, context) => {
  const { companyID } = event.queryStringParameters;
  let allRecords;

  const params = {
    TableName: process.env.ANALYSE_TABLE_NAME,
    IndexName: "companyAndCreatedAt",
    KeyConditionExpression: "#companyID = :companyIdentify",
    ExpressionAttributeValues: {
      ":companyIdentify": companyID,
    },
  };
  try {
    const result = await dynamodb.query(params).promise();
    allRecords = result.Items;
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(allRecords),
  };
};

export const handler = commonMiddleware(getParticularCompanyRecords);
