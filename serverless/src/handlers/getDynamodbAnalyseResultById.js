import AWS from "aws-sdk";
import commonMiddleware from "../lib/commonMiddleware";
import createError from "http-errors";

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function getRecordById(id) {
  let record;
  try {
    const result = await dynamodb
      .get({
        TableName: process.env.ANALYSE_TABLE_NAME,
        Key: { id },
      })
      .promise();
    record = result.Item;
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  // if item by this id is falsy
  if (!record) {
    throw new createError.NotFound(`Analyse item with ID ${id} not found!`);
  }

  return record;
}

const getDynamodbAnalyseResultById = async (event, context) => {
  const { id } = event.pathParameters;

  const record = await getRecordById(id);

  return {
    statusCode: 200,
    body: JSON.stringify(record),
  };
};

export const handler = commonMiddleware(getDynamodbAnalyseResultById);
