import AWS from "aws-sdk";
import commonMiddleware from "../lib/commonMiddleware";
import createError from "http-errors";
import { getRecordById } from "./getDynamodbAnalyseResultById";

const dynamodb = new AWS.DynamoDB.DocumentClient();

const patchDynamodbAnalyseResultSuburbById = async (event, context) => {
  const { id } = event.pathParameters;
  const suburb = event.body;

  await getRecordById(id);

  const params = {
    TableName: process.env.ANALYSE_TABLE_NAME,
    Key: { id },
    UpdateExpression: "set suburb = :newValue",
    ExpressionAttributeValues: {
      ":newValue": suburb
    },
    ReturnValues: "ALL_NEW"
  };

  let updatedRecord;

  try {
    const result = await dynamodb.update(params).promise();
    updatedRecord = result.Attributes;
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(updatedRecord),
  };
};

export const handler = commonMiddleware(patchDynamodbAnalyseResultSuburbById);
