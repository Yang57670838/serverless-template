// get last 24 hours by createdAt and in which company by companyID
import moment from "moment";
import createError from "http-errors";
import { getCompanyRecordsFromDaytime } from "../lib/getCompanyRecordsFromDay";

const nightlyAnalyseReport = async (event, context) => {
  const oneDayAgoUTC = moment().add(-1, "days").toISOString(); // 24 hours ago actually
  const companyID = "12345";

  try {
    const allItemsFrom24Hours = await getCompanyRecordsFromDaytime(
      companyID,
      oneDayAgoUTC
    );
    console.log("allItemsFrom24Hours", allItemsFrom24Hours);
    // TODO: sns send email notice for report
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
};

export const handler = nightlyAnalyseReport;
