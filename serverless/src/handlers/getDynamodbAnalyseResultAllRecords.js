import AWS from "aws-sdk";
import commonMiddleware from "../lib/commonMiddleware";
import createError from "http-errors";

const dynamodb = new AWS.DynamoDB.DocumentClient();

const getDynamodbAnalyseResultAllRecords = async (event, context) => {
  let allRecords;
  try {
    // since its scan, try not do it, can be cost..
    const result = await dynamodb.scan({ TableName: process.env.ANALYSE_TABLE_NAME}).promise();
    allRecords = result.Items;
  } catch(error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(allRecords),
  };
};

export const handler = commonMiddleware(getDynamodbAnalyseResultAllRecords);
