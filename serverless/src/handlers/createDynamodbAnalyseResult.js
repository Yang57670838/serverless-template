import { v4 as uuid } from "uuid";
import AWS from "aws-sdk";
import commonMiddleware from "../lib/commonMiddleware";
import createError from "http-errors";

const dynamodb = new AWS.DynamoDB.DocumentClient();

const createDynamodbAnalyseResult = async (event, context) => {
  const { suburb, username, email, companyID } = event.body;
  const now = new Date();
  const result = {
    id: uuid(),
    suburb,
    username,
    email,
    companyID,
    createdAt: now.toISOString(),
  };

  try {
    await dynamodb
    .put({
      TableName: process.env.ANALYSE_TABLE_NAME,
      Item: result,
    })
    .promise();
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 201,
    body: JSON.stringify(result),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

export const handler = commonMiddleware(createDynamodbAnalyseResult);
