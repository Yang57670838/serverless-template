// cold start init
const config = SomeConfigService.loadAndParseConfig();
const db = SomeDBService.connectToDB(config.dbName);

exports.handler = async (event, context) => {
    const results = await db.loadDataFromTable(config.tableName);
    return results;
}