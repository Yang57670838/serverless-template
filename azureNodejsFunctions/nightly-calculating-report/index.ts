import { AzureFunction, Context } from "@azure/functions"

const timerTrigger: AzureFunction = async function (context: Context, myTimer: any): Promise<void> {
    var timeStamp = new Date().toISOString();
    
    context.bindings.outputBlob = "Reports for " + timeStamp;
    context.log('every first day of this month report..', timeStamp);   
};

export default timerTrigger;
