import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as iam from 'aws-cdk-lib/aws-iam';

export class UploadS3StoreDynamodbCdkStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

     // IAM service role
     const iamRoleForLambda = new iam.Role(this, "iamLambdaRole", {
      roleName: "storeS3FilenameRole",
      description: "role for lambda to store filename from S3 into dynamodb",
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com')
    })
    iamRoleForLambda.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonDynamoDBFullAccess'));
    // give it access to S3, in case it need to access S3 to read file..
    iamRoleForLambda.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonS3FullAccess'));
    // give it permission to write cloudwatch log..
    iamRoleForLambda.addManagedPolicy(iam.ManagedPolicy.fromAwsManagedPolicyName('CloudWatchFullAccess'));
  }
}
