# Purpose

someone upload files to s3, will record the file name in dynamodb

## cdk contains

IAM role, S3, S3 event notification, lambda, DynamoDB

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
